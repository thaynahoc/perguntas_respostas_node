const express = require("express");
const app = express();
var port = 4000;

//Express utilizar o ejs como view engine, ou seja, motor HTML
app.set('view engine', 'ejs');

//Utilizar arquivos estáticos (imagens, css, js)
app.use(express.static('public'));

app.get('/:nome/:idade', function (req, res) { 
    var nome = req.params.nome;
    var idade = req.params.idade;
    var exibirMsg = true;
    var produtos = [
        { nome: "Doritos", preco: 3.14},
        { nome: "PS5", preco: 6000.45 },
        { nome: "Galaxy S9", preco: 1700.60 },
    ];

    res.render('index', {
        nome: nome,
        idade: idade,
        msg: exibirMsg,
        produtos: produtos
    });
 })

app.listen(port, () => { console.log('SERVER UP');} );