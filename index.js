const express = require("express");
const bodyParser = require("body-parser");
const app = express();
var port = 4000;
const PerguntaModel = require('./database/Pergunta');
const RespostaModel = require('./database/Resposta');
const conn = require('./database/database');

conn.authenticate().then(() => {
    console.log("Conexão OK");
}).catch(() => {
    console.log("Conexão Falhou");    
});

//Express utilizar o ejs como view engine, ou seja, motor HTML
app.set('view engine', 'ejs');

//Utilizar arquivos estáticos (imagens, css, js)
app.use(express.static('public'));
//Utilizar o bodyParser para transformar os dados enviados nas requisições
app.use(bodyParser.urlencoded({ extended:false}));
//Dados via JSON
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.render('index');
});

app.get('/perguntas', function (req, res) {
    PerguntaModel.findAll({raw:true, order: [['id', 'desc']]}).then(perguntas => {
        res.render('perguntas', {
            perguntas: perguntas
        });
    });
});

app.get('/pergunta/:id', function (req, res) {
    var id = req.params.id;
    PerguntaModel.findOne({
        where: {id: id}
    }).then(pergunta => {
        if (pergunta != undefined) {

            RespostaModel.findAll({
                where: { pergunta_id: pergunta.id },
                order: [ ['id', 'desc'] ]
            }).then(respostas => {
                res.render('pergunta', {
                    pergunta: pergunta,
                    respostas: respostas
                });
            });

        } else {
            res.redirect('/perguntas');
        }
    });
});

app.post('/saveQuestion', function (req, res) {
    var titulo = req.body.titulo;
    var descricao = req.body.descricao;
    PerguntaModel.create({
        titulo: titulo,
        descricao: descricao 
    }).then(() => {
        res.redirect('/perguntas');
    });
    // res.send('Recebido: Título = '+titulo+' | Descrição: '+ descricao);
});

app.post('/saveAnswer', function (req, res) {
    var id_pergunta = req.body.id_pergunta;
    var resposta = req.body.resposta;
    RespostaModel.create({
        pergunta_id: id_pergunta,
        resposta: resposta
    }).then(resposta => {
        if (resposta != undefined) {
            res.redirect('/pergunta/' + id_pergunta);
        } else {
            res.redirect('/perguntas');
        }
    });
});

app.listen(port, () => {
    console.log('SERVER UP');
});