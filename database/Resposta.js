const Sequelize = require('sequelize');
const conn = require('./database');

const Resposta = conn.define('resposta', {
    resposta: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    pergunta_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

Resposta.sync({ force: false }).then(() => {
    console.log('Tabela de resposta ok');
});

module.exports = Resposta;